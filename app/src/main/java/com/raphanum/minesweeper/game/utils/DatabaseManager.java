package com.raphanum.minesweeper.game.utils;

import com.raphanum.minesweeper.game.model.Score;

import java.util.List;

public interface DatabaseManager<T extends Score> {

    void save(String name, int score);

    List<T> getHighScores();
}
