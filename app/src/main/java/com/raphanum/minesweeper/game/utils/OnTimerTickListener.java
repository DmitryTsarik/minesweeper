package com.raphanum.minesweeper.game.utils;

public interface OnTimerTickListener {
    void tick();
}
