package com.raphanum.minesweeper.game;

public interface MinesweeperView {

    void showWin();

    void showLoose();

    void showHighScores();

    void updateField(byte[][] fieldState);

    void updateScores(int scores);

    void updateTime(int seconds);
}
