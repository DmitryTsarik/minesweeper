package com.raphanum.minesweeper.game.model;

public interface Score {

    String getName();

    int getScoreValue();
}
