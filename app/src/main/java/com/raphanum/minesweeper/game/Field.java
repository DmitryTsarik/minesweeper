package com.raphanum.minesweeper.game;

import java.util.Random;

public class Field
{
    public static final byte OPENED = 0;
    public static final byte CLOSED = 1;
    public static final int NO_NEIGHBOUR_MINES = 0;
    public static final int ONE_NEIGHBOUR_MINE = 1;
    public static final int TWO_NEIGHBOUR_MINES = 2;
    public static final int THREE_NEIGHBOUR_MINES = 3;
    public static final int FOUR_NEIGHBOUR_MINES = 4;
    public static final int FIVE_NEIGHBOUR_MINES = 5;
    public static final int SIX_NEIGHBOUR_MINES = 6;
    public static final int SEVEN_NEIGHBOUR_MINES = 7;
    public static final int EIGHT_NEIGHBOUR_MINES = 8;
    public static final int MINE = 9;
    private int rows;
    private int columns;
    private int mines;
    private int[][] minesField;
    private byte[][] fieldState;
    private boolean mineOpened;

    Field(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        mineOpened = false;
        mines = rows * columns / 10;
        initField();
    }

    private void initField() {
        minesField = new int[rows][columns];
        fieldState = new byte[rows][columns];
        initFieldState();
        setMines();
    }

    private void setMines() {
        Random random = new Random();
        for (int i = 0; i < mines; i++) {
            int row = random.nextInt(rows);
            int column = random.nextInt(columns);

            if (minesField[row][column] != MINE) {
                minesField[row][column] = MINE;
                calculateFieldAround(row, column);
            } else {
                i--;
            }
        }
    }

    private void calculateFieldAround(int row, int column) {
        setCell(row - 1, column - 1);
        setCell(row - 1, column);
        setCell(row - 1, column + 1);
        setCell(row, column - 1);
        setCell(row, column + 1);
        setCell(row + 1, column - 1);
        setCell(row + 1, column);
        setCell(row + 1, column + 1);
    }

    private void setCell(int row, int column) {
        try {
            if (minesField[row][column] != MINE) {
                minesField[row][column]++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            //ignore
        }
    }

    void openCell(int row, int column) {
        try {
            if (fieldState[row][column] == CLOSED) {
                fieldState[row][column] = OPENED;
                if (minesField[row][column] == MINE) {
                    mineOpened = true;
                } else if (minesField[row][column] > 0) {
                    // если открылась ячейка с числом, то открываем только её
                } else {
                    // если открылась пустая ячейка, то открываем все соседние
                    openCell(row - 1, column - 1);
                    openCell(row - 1, column);
                    openCell(row - 1, column + 1);
                    openCell(row, column - 1);
                    openCell(row, column + 1);
                    openCell(row + 1, column - 1);
                    openCell(row + 1, column);
                    openCell(row + 1, column + 1);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // ignore
        }
    }

    int getOpenedCellsCount() {
        int count = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                count += getFieldState()[i][j];
            }
        }
        return count;
    }

    void initFieldState() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                fieldState[i][j] = CLOSED;
            }
        }
    }

    public int[][] getMinesField() {
        return minesField;
    }

    public byte[][] getFieldState() {
        return fieldState;
    }

    boolean isMineOpened() {
        return mineOpened;
    }

    void setMineOpened(boolean mineOpened) {
        this.mineOpened = mineOpened;
    }

    boolean isClear() {
        return getOpenedCellsCount() == mines;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public int getMines() {
        return mines;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                sb.append(" ").append(minesField[i][j]);
            }
            sb.append("\n");
        }

        sb.append("Opened minesField:\n");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                sb.append(" ").append(fieldState[i][j]);
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
