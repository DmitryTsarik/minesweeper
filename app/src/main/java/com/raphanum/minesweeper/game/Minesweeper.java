package com.raphanum.minesweeper.game;

import com.raphanum.minesweeper.game.utils.DatabaseManager;
import com.raphanum.minesweeper.game.utils.MinesweeperTimerTask;
import com.raphanum.minesweeper.game.utils.OnTimerTickListener;

import java.util.Random;
import java.util.Timer;

public class Minesweeper {
    private Field field;
    private String name;
    private int scores;
    private MinesweeperView view;
    private Timer timer;
    private boolean firstClick;
    private int time;
    private DatabaseManager databaseManager;

    public Minesweeper(MinesweeperView view, String name) {
        this.view = view;
        this.name = name;
        startNewGame();
    }

    private void init() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        firstClick = true;
        time = 0;
    }

    public void startNewGame() {
        init();
        Random random = new Random();
        field = new Field(random.nextInt(15) + 15, random.nextInt(5) + 10);
    }

    public void restart() {
        init();
        field.setMineOpened(false);
        field.initFieldState();
    }

    public void onCellClick(int row, int column) {
        if (firstClick) {
            firstClick = false;
            timer.schedule(new MinesweeperTimerTask(new OnTimerTickListener() {
                @Override
                public void tick() {
                    time++;
                    view.updateTime(time);
                }
            }), 1000, 1000);
        }
        field.openCell(row, column);
        view.updateField(field.getFieldState());
        if (!field.isMineOpened()) {
            calculateScores();
            view.updateScores(scores);
            if (field.isClear()) {
                timer.cancel();
                saveScore();
                view.showWin();
            }
        } else {
            timer.cancel();
            view.showLoose();
        }
    }

    private void saveScore() {
        if (databaseManager != null) {
            databaseManager.save(name, scores);
        }
    }

    public void onCellClick(int position) {
        onCellClick(position / field.getColumns(), position % field.getColumns());
    }

    private void calculateScores() {
        scores = field.getRows() * field.getColumns() - field.getOpenedCellsCount();
    }

    public Field getField() {
        return field;
    }

    public void getHighScores() {
        view.showHighScores();
    }

    public int getScores() {
        return scores;
    }

    public void setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    @Override
    public String toString() {
        return field.toString();
    }
}