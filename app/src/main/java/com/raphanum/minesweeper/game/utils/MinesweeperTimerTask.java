package com.raphanum.minesweeper.game.utils;

import java.util.TimerTask;

public class MinesweeperTimerTask extends TimerTask {
    private OnTimerTickListener onTimerTickListener;

    public MinesweeperTimerTask(OnTimerTickListener onTimerTickListener) {
        this.onTimerTickListener = onTimerTickListener;
    }

    @Override
    public void run() {
        onTimerTickListener.tick();
    }
}
