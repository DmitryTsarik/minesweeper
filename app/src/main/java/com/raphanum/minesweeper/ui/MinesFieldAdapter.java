package com.raphanum.minesweeper.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.raphanum.minesweeper.R;
import com.raphanum.minesweeper.game.Field;

public class MinesFieldAdapter extends BaseAdapter {
    private int[] minesField;
    private byte[] fieldState;
    private int rows;
    private int columns;
    private Context context;

    public MinesFieldAdapter(Context context, Field field) {
        this.context = context;
        setData(field);
    }

    public void setData(Field field) {
        rows = field.getRows();
        columns = field.getColumns();
        initMinesField(field);
        initFieldState();
        notifyDataSetChanged();
    }

    private int getImageResource(int position) {
        if (fieldState[position] == Field.CLOSED) {
            return R.drawable.cell_closed;
        } else {
            switch (minesField[position]) {
                case Field.MINE:
                    return R.drawable.mine;
                case Field.NO_NEIGHBOUR_MINES:
                    return R.drawable.cell_empty;
                case Field.ONE_NEIGHBOUR_MINE:
                    return R.drawable.cell_1;
                case Field.TWO_NEIGHBOUR_MINES:
                    return R.drawable.cell_2;
                case Field.THREE_NEIGHBOUR_MINES:
                    return R.drawable.cell_3;
                case Field.FOUR_NEIGHBOUR_MINES:
                    return R.drawable.cell_4;
                case Field.FIVE_NEIGHBOUR_MINES:
                    return R.drawable.cell_5;
                case Field.SIX_NEIGHBOUR_MINES:
                    return R.drawable.cell_6;
                case Field.SEVEN_NEIGHBOUR_MINES:
                    return R.drawable.cell_7;
                case Field.EIGHT_NEIGHBOUR_MINES:
                    return R.drawable.cell_8;
            }
        }
        return 0;
    }

    private void initMinesField(Field field) {
        minesField = new int[rows * columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                minesField[i * columns + j] = field.getMinesField()[i][j];
            }
        }
    }

    private void initFieldState() {
        fieldState = new byte[minesField.length];
        for (int i = 0; i < fieldState.length; i++) {
            fieldState[i] = Field.CLOSED;
        }
    }

    public void updateFieldState(byte[][] state) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                fieldState[i * columns + j] = state[i][j];
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return minesField.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SquareImageView imageView = new SquareImageView(context);
        imageView.setImageResource(getImageResource(i));
        return imageView;
    }
}
