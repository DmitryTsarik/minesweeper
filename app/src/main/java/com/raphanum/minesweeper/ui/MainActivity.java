package com.raphanum.minesweeper.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.raphanum.minesweeper.DatabaseManagerImpl;
import com.raphanum.minesweeper.R;
import com.raphanum.minesweeper.game.Minesweeper;
import com.raphanum.minesweeper.game.MinesweeperView;

public class MainActivity extends AppCompatActivity implements MinesweeperView {
    public static final String NAME = "name";
    private Minesweeper minesweeper;
    private GridView fieldGridView;
    private MinesFieldAdapter fieldAdapter;
    private TextView scoresView;
    private TextView timerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        minesweeper = new Minesweeper(this, getIntent().getStringExtra(NAME));
        minesweeper.setDatabaseManager(new DatabaseManagerImpl());

        fieldAdapter = new MinesFieldAdapter(this, minesweeper.getField());
        fieldGridView = (GridView) findViewById(R.id.field_view);
        fieldGridView.setAdapter(fieldAdapter);
        fieldGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                minesweeper.onCellClick(i);
            }
        });

        scoresView = (TextView) findViewById(R.id.scores_view);

        timerView = (TextView) findViewById(R.id.timer_view);
        initGame();
    }

    private void initGame() {
        scoresView.setText("0");
        timerView.setText("00:00");
        fieldGridView.setNumColumns(minesweeper.getField().getColumns());
        fieldAdapter.setData(minesweeper.getField());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_top_ten:
                showTopTenDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //ignore
    }

    @Override
    public void showWin() {
        showDialog(getString(R.string.you_win), getString(R.string.total_score) + minesweeper.getScores());
    }

    @Override
    public void showLoose() {
        showDialog(getString(R.string.you_loose), getString(R.string.loose_message));
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                minesweeper.getHighScores();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showTopTenDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.top_10))
                .setCancelable(true)
                .setView(getTopTenRecyclerView());
        AlertDialog alert = builder.create();
        alert.show();
    }

    private RecyclerView getTopTenRecyclerView() {
        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new ScoresAdapter(new DatabaseManagerImpl().getHighScores()));
        return recyclerView;
    }

    @Override
    public void showHighScores() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.best)
                .setCancelable(false)
                .setView(getTopTenRecyclerView())
                .setPositiveButton(R.string.new_game, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        minesweeper.startNewGame();
                        initGame();

                    }
                })
                .setNegativeButton(R.string.restart,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                minesweeper.restart();
                                initGame();
                            }
                        })
                .setNeutralButton(R.string.quit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finishAffinity();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void updateField(byte[][] fieldState) {
        fieldAdapter.updateFieldState(minesweeper.getField().getFieldState());
    }

    @Override
    public void updateScores(int scores) {
        scoresView.setText(String.valueOf(scores));
    }

    @Override
    public void updateTime(int seconds) {
        final int minutes = seconds/60;
        final int s = seconds % 60;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timerView.setText(String.format("%02d:%02d", minutes, s));
            }
        });
    }
}
