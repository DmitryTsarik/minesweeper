package com.raphanum.minesweeper.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raphanum.minesweeper.R;
import com.raphanum.minesweeper.ScoreRealm;

import java.util.List;

public class ScoresAdapter extends RecyclerView.Adapter<ScoresAdapter.ScoreViewHolder> {

    private List<ScoreRealm> list;

    public ScoresAdapter(List<ScoreRealm> list) {
        this.list = list;
    }

    @Override
    public ScoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.score_item, parent, false);
        return new ScoreViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ScoreViewHolder holder, int position) {
        holder.number.setText(String.valueOf(position + 1));
        holder.name.setText(list.get(position).getName());
        holder.score.setText(String.valueOf(list.get(position).getScoreValue()));
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public static class ScoreViewHolder extends RecyclerView.ViewHolder {
        private TextView number;
        private TextView name;
        private TextView score;

        public ScoreViewHolder(View itemView) {
            super(itemView);
            number = (TextView) itemView.findViewById(R.id.item_number);
            name = (TextView) itemView.findViewById(R.id.item_name);
            score = (TextView) itemView.findViewById(R.id.item_score);
        }
    }
}
