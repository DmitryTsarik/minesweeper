package com.raphanum.minesweeper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.raphanum.minesweeper.R;

public class StartActivity extends AppCompatActivity {
    private Button startButton;
    private EditText nameEdit;
    private TextWatcher textWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start);

        initStartButton();
        initNameEdit();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        nameEdit.addTextChangedListener(textWatcher);
    }

    @Override
    protected void onPause() {
        super.onPause();
        nameEdit.removeTextChangedListener(textWatcher);
    }

    private void initStartButton() {
        startButton = (Button) findViewById(R.id.start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartActivity.this, MainActivity.class);
                intent.putExtra(MainActivity.NAME, nameEdit.getText().toString());
                startActivity(intent);
            }
        });
    }

    private void initNameEdit() {
        nameEdit = (EditText) findViewById(R.id.name_edit);
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    startButton.setEnabled(true);
                } else {
                    startButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }
}
