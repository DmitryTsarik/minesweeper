package com.raphanum.minesweeper;

import com.raphanum.minesweeper.game.model.Score;

import io.realm.RealmObject;

public class ScoreRealm extends RealmObject implements Score {
    private int scoreValue;
    private String name;

    public ScoreRealm() {
    }

    public ScoreRealm(String name, int scoreValue) {
        this.scoreValue = scoreValue;
        this.name = name;
    }

    @Override
    public int getScoreValue() {
        return scoreValue;
    }

    public void setScoreValue(int scoreValue) {
        this.scoreValue = scoreValue;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
