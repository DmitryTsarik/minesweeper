package com.raphanum.minesweeper;

import com.raphanum.minesweeper.game.utils.DatabaseManager;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class DatabaseManagerImpl implements DatabaseManager<ScoreRealm> {
    private Realm realm;

    public DatabaseManagerImpl() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void save(final String name, final int score) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(new ScoreRealm(name, score));
            }
        });
    }

    @Override
    public List<ScoreRealm> getHighScores() {
        final RealmResults<ScoreRealm> results = realm.where(ScoreRealm.class).findAllSorted("scoreValue", Sort.DESCENDING);
        if (results.size() > 10) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    results.deleteLastFromRealm();
                }
            });
        }
        return results.subList(0, results.size());
    }
}
